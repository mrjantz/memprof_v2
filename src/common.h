#ifndef __COMMON_H__
#define __COMMON_H__

#include <stdint.h>
#include <time.h>
#include <sys/time.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>

#define MAX_PROFILE_EVENTS 2

#define _XSTR(x) #x
#define XSTR(x) _XSTR(x)

#define PLINE(...) \
    (printf(XSTR(__FILE__) ":" XSTR(__LINE__) ":\n" __VA_ARGS__), fflush(stdout))

#define TIMED_BEGIN { u64 __start = measure_time_now_ms();
#define TIMED_END(name) PLINE(XSTR(name) ": %lums\n", measure_time_now_ms() - __start); }

#define likely(x)   (__builtin_expect(!!(x), 1))
#define unlikely(x) (__builtin_expect(!!(x), 0))

#define MIN(a, b) ((a) <= (b) ? (a) : (b))
#define MAX(a, b) ((a) >= (b) ? (a) : (b))

#define UINT(w) uint##w##_t
#define SINT(w) int##w##_t

#define u8  UINT(8 )
#define u16 UINT(16)
#define u32 UINT(32)
#define u64 UINT(64)

#define i8  SINT(8 )
#define i16 SINT(16)
#define i32 SINT(32)
#define i64 SINT(64)

#define KB(x) (x/(1ull<<10))
#define MB(x) (x/(1ull<<20))
#define GB(x) (x/(1ull<<30))

#define PAGE_SIZE (4096)

typedef void *addr_t;

#define UTIL_FN_PRELUDE static __inline__

UTIL_FN_PRELUDE
u64 next_power_of_2(u64 x) {
    if (x == 0) {
        return 2;
    }

    x--;
    x |= x >> 1;
    x |= x >> 2;
    x |= x >> 4;
    x |= x >> 8;
    x |= x >> 16;
    x |= x >> 32;
    x++;

    return x;
}

UTIL_FN_PRELUDE
u64 measure_time_now_ms(void) {
    struct timeval tv;

    gettimeofday(&tv, NULL);

    return 1000ULL * tv.tv_sec + (tv.tv_usec / 1000ULL);
}

UTIL_FN_PRELUDE
u64 sum_u64_array(u64 *x, int len) {
    int i;
    u64 sum;

    sum = 0;
    for ( i = 0; i < len; i++) {
        sum += x[i];
    }
    return sum;
}

UTIL_FN_PRELUDE
float sum_float_array(float *x, int len) {
    int i;
    float sum;

    sum = 0.0;
    for ( i = 0; i < len; i++) {
        sum += x[i];
    }
    return sum;
}

UTIL_FN_PRELUDE
int create_fifo(char *fifo) {
    int err;

try_again:
    err = mkfifo(fifo, 0666);
    if (err) {
        if (errno == EEXIST) {
            if (unlink(fifo) < 0) {
                fprintf(stderr, "could not remove fifo: %s\n",
                        strerror(errno));
                return errno;
            }
            goto try_again;
        }
        fprintf(stderr, "could not create fifo: %s\n",
                strerror(errno));
        return errno;
    }
    return 0;
}

UTIL_FN_PRELUDE
u64 safe_diff(u64 x, u64 y) {
    return ((x > y) ? (x - y) : ((u64)0));
}

#endif
