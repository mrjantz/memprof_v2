#include <vmlinux.h>
#include <bpf/bpf_helpers.h>
#include <bpf/bpf_tracing.h>
#include <asm-generic/errno.h>
#include "memprof.h"

/* found these in kernel headers and configs for v. 5.7.2 -- not portable to
 * other kernels / configurations
 */
#define MAX_PROC_ENTRIES    256
#define PAGE_POISON_PATTERN -11
#define SECTIONS_WIDTH      0
#define NODES_WIDTH         6
#define NODES_MASK          ((1UL << NODES_WIDTH) - 1)
#define SECTIONS_PGOFF      ((sizeof(unsigned long)*8) - SECTIONS_WIDTH)
#define NODES_PGOFF         (SECTIONS_PGOFF - NODES_WIDTH)
#define NODES_PGSHIFT       (NODES_PGOFF * (NODES_WIDTH != 0))

struct bpf_map_def SEC("maps") debug_control = {
    .type = BPF_MAP_TYPE_ARRAY,
    .key_size = sizeof(__u32),
    .value_size = sizeof(__u32),
    .max_entries = 1,
};

struct bpf_map_def SEC("maps") tmp_addrs = {
    .type = BPF_MAP_TYPE_PERCPU_ARRAY,
    .key_size = sizeof(__u32),
    .value_size = sizeof(__u64),
    .max_entries = 1,
};

struct bpf_map_def SEC("maps") in_handle_mm_fault = {
    .type = BPF_MAP_TYPE_PERCPU_ARRAY,
    .key_size = sizeof(__u32),
    .value_size = sizeof(__u32),
    .max_entries = 1,
};

struct bpf_map_def SEC("maps") in_unmap = {
    .type = BPF_MAP_TYPE_PERCPU_ARRAY,
    .key_size = sizeof(__u32),
    .value_size = sizeof(__u32),
    .max_entries = 1,
};

struct bpf_map_def SEC("maps") bpf_procs = {
    .type = BPF_MAP_TYPE_HASH_OF_MAPS,
    .key_size = sizeof(__u32),
    .value_size = sizeof(__u32),
    .max_entries = MAX_PROCS,
};

static __always_inline
enum numa_node_id page_to_nid(const struct page *page)
{
    struct page *p = (struct page *)page;

    if (p->flags == PAGE_POISON_PATTERN) {
        return NR_NUMA_NODES;
    }

    return ( (enum numa_node_id)
      ( ((p->flags) >> NODES_PGSHIFT) & NODES_MASK ) );
}

static __always_inline
struct bpf_region_record* region_map_lookup_or_try_init( int *map, u64 *key ) {
    struct bpf_region_record *rec;
    struct bpf_region_record new_rec;
    long err;

    rec = bpf_map_lookup_elem(map, key);
    if (rec) {
        return rec;
    }

    __builtin_memset(&new_rec, 0, sizeof(struct bpf_region_record));
    err = bpf_map_update_elem(map, key, &new_rec, BPF_NOEXIST);
    if (err && err != -EEXIST) {
        return ((struct bpf_region_record*)0x0);
    }

    return bpf_map_lookup_elem(map, key);
}

static __always_inline
int record_llc_miss( int *map, enum llc_miss_event evt,
    struct bpf_perf_event_data *ctx ) {
    u64  key;
    struct bpf_region_record *rec;

    if (!ctx) {
        return 0;
    }

#ifdef MEMPROF_REGION_PROFILE
    key = (ctx->addr >> MEMPROF_REGION_SHIFT);
    rec = region_map_lookup_or_try_init(map, &key);
    if (rec) {
        if (evt >= NR_MISS_EVENTS) {
            evt = 0;
        }
        rec->llc_misses[evt] += 1;
    }
#else
#endif

    return 0;
}

static __always_inline
int on_llc_miss( struct bpf_perf_event_data *ctx,
    enum llc_miss_event evt ) {
    int   key;
    int  *region_map;

    key = ( ( bpf_get_current_pid_tgid() >> 32 ) );
    region_map = bpf_map_lookup_elem(&bpf_procs, &key);
    if (!region_map) {
        return 0;
    }

    return record_llc_miss(region_map, evt, ctx);
}

static __always_inline
int add_page_to_rss( int *map, u64 addr, enum numa_node_id nid ) {
    u64 key;
    struct bpf_region_record *rec;

#ifdef MEMPROF_REGION_PROFILE
    key = (addr >> MEMPROF_REGION_SHIFT);
    rec = region_map_lookup_or_try_init(map, &key);
    if (rec) {
        if (nid >= NR_NUMA_NODES) {
            return 0;
        }
        rec->alloc_pages[nid] += 1;
    }
#else
#endif

    return 0;
}

static __always_inline
int remove_page_from_rss( int *map, u64 addr, enum numa_node_id nid ) {
    u64 key;
    struct bpf_region_record *rec;

#ifdef MEMPROF_REGION_PROFILE
    key = (addr >> MEMPROF_REGION_SHIFT);
    rec = region_map_lookup_or_try_init(map, &key);
    if (rec) {
        if (nid >= NR_NUMA_NODES) {
            return 0;
        }
        //__sync_fetch_and_add(&pg_remove_cnt, 1);
        //__sync_fetch_and_add(&(rec->free_pages[nid]), 1);
        rec->free_pages[nid] += 1;
    }
#else
#endif

    return 0;
}

SEC("perf_event/1")
int on_ddr_miss(struct bpf_perf_event_data *ctx)
{
    return on_llc_miss(ctx, DDR_MISS_EVENT);
}

SEC("perf_event/2")
int on_pmm_miss(struct bpf_perf_event_data *ctx)
{
    return on_llc_miss(ctx, PMM_MISS_EVENT);
}

SEC("fentry/handle_mm_fault")
int BPF_PROG(handle_mm_fault_entry)
{
    u64 pid_tgid;
    u32 pid, tgid, zero;

    pid_tgid = bpf_get_current_pid_tgid();

    tgid = ( pid_tgid >> 32 );
    if (!(bpf_map_lookup_elem(&bpf_procs, &tgid))) {
        return 0;
    }

    zero = 0;
    pid = ( (u32) pid_tgid );
    bpf_map_update_elem(&in_handle_mm_fault, &zero, &pid, BPF_ANY);
    return 0;
}

SEC("fexit/handle_mm_fault")
int BPF_PROG(handle_mm_fault_exit)
{
    u32 zero;
 
    /* in the common case, tmp_addrs will already be cleared, but we still
     * need to clear it here in case of an error condition
     */
    zero = 0;
    bpf_map_delete_elem(&tmp_addrs, &zero);
    bpf_map_delete_elem(&in_handle_mm_fault, &zero);
    return 0;
}

SEC("fentry/alloc_pages_vma")
int BPF_PROG(alloc_pages_vma_entry, gfp_t gfp, int order,
    struct vm_area_struct *vma, unsigned long addr)
{
    u32   pid, zero, *exp;

    zero = 0;
    exp = bpf_map_lookup_elem(&in_handle_mm_fault, &zero);
    if (!exp) {
        return 0;
    }

    pid = bpf_get_current_pid_tgid();
    if (pid != (*exp)) {
        return 0;
    }

    bpf_map_update_elem(&tmp_addrs, &zero, &addr, BPF_ANY);
    return 0;
}

SEC("fexit/alloc_pages_vma")
int BPF_PROG(alloc_pages_vma_return, gfp_t gfp, int order,
    struct vm_area_struct *vma, unsigned long arg_addr, int node,
    bool hugepage, struct page *page)
{
    u64 pid_tgid, *addr;
    u32 pid, tgid, zero, *exp;
    int *region_map;
    enum numa_node_id nid;

    pid_tgid = bpf_get_current_pid_tgid();

    tgid = ( pid_tgid >> 32 );
    region_map = bpf_map_lookup_elem(&bpf_procs, &tgid);
    if (!region_map) {
        return 0;
    }

    zero = 0;
    exp = bpf_map_lookup_elem(&in_handle_mm_fault, &zero);
    if (!exp) {
        return 0;
    }

    pid = ( (u32) pid_tgid );
    if (pid != (*exp)) {
        return 0;
    }

    addr = bpf_map_lookup_elem(&tmp_addrs, &zero);
    if (!addr) {
        return 0;
    }

    if (page) {
        nid = page_to_nid(page);
        if (nid < 0) {
            return 0;
        }

        add_page_to_rss(region_map, (*addr), nid);
    }

    bpf_map_delete_elem(&tmp_addrs, &zero);
    return 0;
}

/* unmap_page_range is the path taken by munmap -- we will need to cover
 * another path for page migration
 */
SEC("fentry/unmap_page_range")
int BPF_PROG(unmap_page_range_entry)
{
    u64 pid_tgid, zero;
    u32 pid, tgid;
    int *region_map;

    pid_tgid = bpf_get_current_pid_tgid();

    tgid = ( pid_tgid >> 32 );
    region_map = bpf_map_lookup_elem(&bpf_procs, &tgid);
    if (!region_map) {
        return 0;
    }

    zero = 0;
    pid = ( (u32) pid_tgid );
    bpf_map_update_elem(&in_unmap, &zero, &pid, BPF_ANY);
    return 0;
}

/* MRJ -- need to use a kprobe because vm_normal_page is not supported by
 * fentry/fexit
 */
SEC("kprobe/vm_normal_page")
int BPF_KPROBE(vm_normal_page_entry, struct vm_area_struct *vma,
    unsigned long addr)
{
    u32 pid, *exp;
    u32 zero;

    zero = 0;
    exp = bpf_map_lookup_elem(&in_unmap, &zero);
    if (!exp) {
        return 0;
    }

    pid = bpf_get_current_pid_tgid();
    if (pid != (*exp)) {
        return 0;
    }

    bpf_map_update_elem(&tmp_addrs, &zero, &addr, BPF_ANY);
    return 0;
}

SEC("fentry/page_remove_rmap")
int BPF_PROG(page_remove_rmap_entry, struct page *page)
{
    u64 pid_tgid, *addr;
    u32 pid, tgid, zero, *exp;
    int *region_map;
    enum numa_node_id nid;

    pid_tgid = bpf_get_current_pid_tgid();

    tgid = ( pid_tgid >> 32 );
    region_map = bpf_map_lookup_elem(&bpf_procs, &tgid);
    if (!region_map) {
        return 0;
    }

    zero = 0;
    exp = bpf_map_lookup_elem(&in_unmap, &zero);
    if (!exp) {
        return 0;
    }

    pid = ( (u32) pid_tgid );
    if (pid != (*exp)) {
        return 0;
    }

    addr = bpf_map_lookup_elem(&tmp_addrs, &zero);
    if (!addr) {
        return 0;
    }

    if (page) {
        nid = page_to_nid(page);
        if (nid < 0) {
            return 0;
        }

        remove_page_from_rss(region_map, (*addr), nid);
    }

    bpf_map_delete_elem(&tmp_addrs, &zero);
    return 0;
}

SEC("fexit/unmap_page_range")
int BPF_PROG(unmap_page_range_exit)
{
    u32 zero;

    
    /* in the common case, tmp_addrs will already be cleared, but we still
     * need to clear it here in case of an error condition
     */
    zero = 0;
    bpf_map_delete_elem(&tmp_addrs, &zero);
    bpf_map_delete_elem(&in_unmap, &zero);
    return 0;
}

char LICENSE[] SEC("license") = "GPL";

#if 0
    int   zero;
    int  *cnt;
    zero = 0;
    cnt = bpf_map_lookup_elem(&debug_control, &zero);
    if (!cnt || ((*cnt) == 1)) {
        return 0;
    }
#endif

#if 0
    (*cnt) -= 1;
    if ((*cnt) > 0) {
        bpf_printk("alloc: %lu remv: %lu\n", alloc_cnt,
                    pg_remove_cnt);
    }
#endif
#if 0
    int   zero;
    int  *cnt;
    zero = 0;
    cnt = bpf_map_lookup_elem(&debug_control, &zero);
    if (!cnt || ((*cnt) == 1)) {
        return 0;
    }
#endif
