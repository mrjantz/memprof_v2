#ifndef __MP_PROFILE_H
#define __MP_PROFILE_H

#include "memprof.h"
#include "array.h"
#include "tree.h"

int start_mp_profile_thread(pthread_t *mp_profile_pthread,
    struct mp_env *env);

void get_region_map_name(pid_t pid, char *buf);

typedef struct {
    u64 llc_misses_val[NR_MISS_EVENTS];
    u64 llc_misses_tot[NR_MISS_EVENTS];
    u64 llc_misses_res[NR_MISS_EVENTS];
    u64 alloc_pages_val[NR_NUMA_NODES];
    u64 alloc_pages_tot[NR_NUMA_NODES];
    u64 alloc_pages_res[NR_NUMA_NODES];
    u64 free_pages_val[NR_NUMA_NODES];
    u64 free_pages_tot[NR_NUMA_NODES];
    u64 free_pages_res[NR_NUMA_NODES];
    float llc_misses_scaled[NR_MISS_EVENTS];
} mp_region;

typedef struct {
    u64 addr;
    mp_region region;
} mp_region_addr;

typedef struct {
    int fd;
    struct bpf_map_info info;
} fd_and_info;

use_tree(u64, mp_region);
typedef tree(u64, mp_region) mp_region_map;
typedef tree_it(u64, mp_region) mp_region_map_it;

use_tree(pid_t, mp_region_map);
typedef tree(pid_t, mp_region_map) mp_profile_map;
typedef tree_it(pid_t, mp_region_map) mp_profile_map_it;

use_tree(pid_t, fd_and_info);
typedef tree(pid_t, fd_and_info) mp_region_fd_map;
typedef tree_it(pid_t, fd_and_info) mp_region_fd_map_it;

extern mp_profile_map    mp_profile;
extern FILE             *profile_file;

extern mp_region_fd_map  region_map_fds;
extern int               bpf_procs_fd;

extern pthread_mutex_t   mp_profile_lock;

int close_and_free_region_map_fd( pid_t pid );
int create_bpf_region_map( pid_t pid );

#endif /* __MP_PROFILE_H */
