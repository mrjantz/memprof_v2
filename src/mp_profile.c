#include <argp.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <asm/unistd.h>
#include <linux/perf_event.h>
#include <pthread.h>

#include "map_helpers.h"
#include "mp_profile.h"

#define LINE_SIZE 512
#define PROFILE_FILE "./profile.txt"
#define MEMPROF_REGION_SHIFT 21
#define MAX_REGIONS 20480
#define PAGE_SHIFT 12

static struct mp_env *env;

mp_profile_map    mp_profile;
FILE             *profile_file;

mp_region_fd_map  region_map_fds;
int               bpf_procs_fd;

pthread_mutex_t   mp_profile_lock;

void get_region_map_name(pid_t pid, char *buf) {
    sprintf(buf, MP_REGION_MAP_STR, pid);
}

float pages_to_MB(u64 pages) {
    return ( (float)(pages << PAGE_SHIFT) / (1024.0*1024.0) );
}

int mp_region_addr_cmp(const void *a, const void *b) {
    int                   i;
    float                 a_scaled, b_scaled;
    const mp_region_addr *na;
    const mp_region_addr *nb;

    na = a;
    nb = b;

    a_scaled = b_scaled = 0.0;
    for (i = 0; i < NR_MISS_EVENTS; i++) {
        a_scaled += na->region.llc_misses_scaled[i];
        b_scaled += nb->region.llc_misses_scaled[i];
    }

    return  (int) ( b_scaled - a_scaled );
}

static void convert_to_flat( mp_region_map *regions,
    array_t *flat_profile ) {
    mp_region_map_it it;
    mp_region_addr new_region_addr;

    tree_traverse((*regions), it) {
        new_region_addr.addr = tree_it_key(it);
        memcpy(&(new_region_addr.region), &(tree_it_val(it)),
               sizeof(mp_region));
        array_push((*flat_profile), new_region_addr); 
    }
}

static int get_region_map_fd_and_info( pid_t pid,
    struct bpf_map_info *info ) {

    fd_and_info        *fdi;
    mp_region_fd_map_it  it;

    it = tree_lookup(region_map_fds, pid);
    if (!(tree_it_good(it))) {
        info = NULL;
        return -1;
    }

    fdi = &(tree_it_val(it));
    memcpy(info, &(fdi->info), sizeof(struct bpf_map_info));
    return fdi->fd;
}

#if 0
static int reset_debug_control(int reset) {
    int fd, err;
    u32 zero;

    zero = 0;
    fd = map_fd_by_name("debug_control");

    err = bpf_map_update_elem(fd, &zero, &reset, 0);
    if (err) {
        fprintf(stderr, "failed to reset debug_control: %d\n", err);
        return err;
    }
    close(fd);

    return 0;
}
#endif

static int reset_bpf_region_map( int region_fd, struct bpf_map_info info,
    u64 *keys, u32 count) {
    int i;
    struct bpf_region_record *reset_rec;

    /* MRJ -- don't know why it's necessary to rewrite the elements to 0, but
     * the values do not get reset if we do not
     */
    reset_rec = (struct bpf_region_record*)
        malloc( env->nr_cpus * info.value_size );
    if (!reset_rec) {
        fprintf(stderr, "could not allocate reset_rec\n");
        return -ENOMEM;
    }

    memset(reset_rec, 0, (env->nr_cpus * info.value_size));
    for (i = 0; i < count; i++) {
        if (bpf_map_update_elem(region_fd, &(keys[i]), reset_rec, 0) < 0) {
            fprintf(stderr, "failed to update region_map: %lx %s\n",
                    keys[i], strerror(errno));
            return errno;
        };
        if (bpf_map_delete_elem(region_fd, &(keys[i])) < 0) {
            fprintf(stderr, "failed to delete from region_map: %lx %s\n",
                    keys[i], strerror(errno));
            return errno;
        };
    }

    return 0;
}

static int get_bpf_region_map( int region_fd, struct bpf_map_info info,
    u64 **keys, struct bpf_region_record **vals, u32 *count) {

    int err;
    u64 *lookup_key;

    (*keys) = (u64*) malloc(MAX_REGIONS * info.key_size);
    if (!keys) {
        fprintf(stderr, "could not allocate keys\n");
        return -ENOMEM;
    }
    memset((*keys), 0, (MAX_REGIONS * info.key_size));

    (*vals) = (struct bpf_region_record*) malloc(
      (MAX_REGIONS * env->nr_cpus * info.value_size));
    if (!vals) {
        fprintf(stderr, "could not allocate vals\n");
        return -ENOMEM;
    }
    memset((*vals), 0, (MAX_REGIONS * env->nr_cpus * info.value_size));

    (*count) = MAX_REGIONS;
    err = dump_hash(region_fd, (*keys), info.key_size, (*vals),
          (info.value_size * env->nr_cpus), count, &lookup_key);
    if (err < 0) {
        fprintf(stderr, "failed to dump region map: %s\n",
                strerror(errno));
    }

    return err;
}

static void add_bpf_region_map_to_profile( pid_t pid, bool reset )
{
    int                        err, i, j, region_fd;
    u32                        count;
    u64                       *keys;
    char                       buf[MAP_NAME_SIZE];
    struct                     bpf_map_info info;

    struct bpf_region_record  *vals, *rec;
    enum llc_miss_event        evt;
    enum numa_node_id          n;

    mp_region                 *region;
    mp_region                  new_region;
    mp_region_map             *mp_regions;
    mp_region_map_it           reg_it;
    mp_profile_map_it          prof_it;

    u64                        llc_misses_all_cpus[NR_MISS_EVENTS];
    u64                        alloc_pages_all_cpus[NR_NUMA_NODES];
    u64                        free_pages_all_cpus[NR_NUMA_NODES];
    u64                        new_total, old_total;

    count = 0;
    keys = NULL;
    vals = NULL;

    get_region_map_name(pid, &(buf[0]));
    region_fd = get_region_map_fd_and_info(pid, &info);
    if (region_fd < 0) {
        fprintf(stderr, "Failed to find region map: %s. %s\n",
                buf, strerror(errno));
        exit(errno);
    }

    err = get_bpf_region_map(region_fd, info, &keys, &vals, &count);
    if (err) {
        fprintf(stderr, "error reading region map: %s\n", buf);
        exit(err);
    };

    if (reset) {
        err = reset_bpf_region_map(region_fd, info, keys, count);
        if (err) {
            fprintf(stderr, "error resetting region map: %s\n", buf);
            exit(err);
        }
    }

    prof_it = tree_lookup(mp_profile, pid);
    if (!tree_it_good(prof_it)) {
        fprintf(stderr, "bad lookup on pid: %d\n", pid);
        exit(-EINVAL);
    }

    mp_regions = &(tree_it_val(prof_it));
    tree_traverse((*mp_regions), reg_it) {
        region = &(tree_it_val(reg_it));
        for (evt = 0; evt < NR_MISS_EVENTS; evt++) {
            region->llc_misses_val[evt]     = ((u64)0);
            region->llc_misses_scaled[evt] *= (1.0 - (env->alpha));
        }
        for (n = 0; n < NR_NUMA_NODES; n++) {
            region->alloc_pages_val[n]      = ((u64)0);
            region->free_pages_val[n]       = ((u64)0);
        }
    }

    for (i = 0; i < count; i++) {

        reg_it = tree_lookup((*mp_regions), keys[i]);
        if (!(tree_it_good(reg_it))) {
            memset(&new_region, 0, sizeof(mp_region));
            reg_it = tree_insert((*mp_regions), keys[i], new_region);
        }

        memset(&llc_misses_all_cpus,  0, (sizeof(u64)*NR_MISS_EVENTS));
        memset(&alloc_pages_all_cpus, 0, (sizeof(u64)*NR_NUMA_NODES));
        memset(&free_pages_all_cpus,  0, (sizeof(u64)*NR_NUMA_NODES));

        region = &(tree_it_val(reg_it));
        for (j = 0; j < (env->nr_cpus); j++) {

            rec = &(vals[((i*(env->nr_cpus)) + j)]);
            for (evt = 0; evt < NR_MISS_EVENTS; evt++) {
                llc_misses_all_cpus[evt] += rec->llc_misses[evt];
            }

            for (n = 0; n < NR_NUMA_NODES; n++) {
                alloc_pages_all_cpus[n] += rec->alloc_pages[n];
                free_pages_all_cpus[n]  += rec->free_pages[n];
            }
        }

        /* set totals using previously recorded reset values
         */
        for (evt = 0; evt < NR_MISS_EVENTS; evt++) {
            old_total = region->llc_misses_tot[evt];
            new_total = (region->llc_misses_res[evt] + llc_misses_all_cpus[evt]);
            if (new_total < old_total) {
                fprintf(stderr, "error: bad total\n");
                exit(-EINVAL);
            }

            region->llc_misses_val[evt] = (new_total - old_total);
            region->llc_misses_tot[evt] = new_total;
            region->llc_misses_scaled[evt] +=
                ((env->alpha) * region->llc_misses_val[evt]);
        }

        for (n = 0; n < NR_NUMA_NODES; n++) {

            old_total = region->alloc_pages_tot[n];
            new_total = (region->alloc_pages_res[n] + alloc_pages_all_cpus[n]);
            if (new_total < old_total) {
                fprintf(stderr, "error: bad total\n");
                exit(-EINVAL);
            }
            region->alloc_pages_val[n] = (new_total - old_total);
            region->alloc_pages_tot[n] = new_total;


            old_total = region->free_pages_tot[n];
            new_total = (region->free_pages_res[n] + free_pages_all_cpus[n]);
            if (new_total < old_total) {
                fprintf(stderr, "error: bad total\n");
                exit(-EINVAL);
            }
            region->free_pages_val[n] = (new_total - old_total);
            region->free_pages_tot[n] = new_total;
        }

        /* record the reset values, if necessary
         */
        if (reset) {
            for (evt = 0; evt < NR_MISS_EVENTS; evt++) {
                region->llc_misses_res[evt] += llc_misses_all_cpus[evt];
            }
            for (n = 0; n < NR_NUMA_NODES; n++) {
                region->alloc_pages_res[n] += alloc_pages_all_cpus[n];
                region->free_pages_res[n]  += free_pages_all_cpus[n];
            }
        }
    }

    free(keys);
    free(vals);
}

static void print_profile(int val, u64 cur_time) {

    pid_t pid;
    float ddr_misses_scaled, pmm_misses_scaled, llc_misses_scaled,
          cur_misses_scaled, cur_misses_ratio, cur_regions_ratio,
          reg_misses_scaled;
    u64   ddr_misses_tot, pmm_misses_tot, llc_misses_tot,
          ddr_misses_val, pmm_misses_val, llc_misses_val,
          total_regions, cur_regions;
    u64   val_alloc_ddr, val_alloc_pmm,
          val_free_ddr, val_free_pmm,
          total_alloc_ddr, total_alloc_pmm,
          total_free_ddr, total_free_pmm,
          total_rss_ddr, total_rss_pmm,
          reg_rss_ddr, reg_rss_pmm;
    mp_profile_map_it prof_it;
    mp_region_addr *reg_addr;
    array_t flat_profile;

    fprintf(profile_file,
            "profile: %8d   time(ms): %12lu\n",
            val, cur_time);

    pthread_mutex_lock(&mp_profile_lock);
    tree_traverse(mp_profile, prof_it) {
        pid = tree_it_key(prof_it);
        fprintf(profile_file, "  PID: %8u\n", pid);

        flat_profile = array_make(mp_region_addr);
        convert_to_flat(&(tree_it_val(prof_it)), &flat_profile);

        qsort(array_data(flat_profile), array_len(flat_profile),
              sizeof(mp_region_addr), mp_region_addr_cmp);

        total_regions     = ((u64)0);
        total_rss_ddr     = total_rss_pmm     = ((u64)0);
        val_alloc_ddr     = val_free_ddr    = ((u64)0);
        val_alloc_pmm     = val_free_pmm    = ((u64)0);
        total_alloc_ddr   = total_free_ddr    = ((u64)0);
        total_alloc_pmm   = total_free_pmm    = ((u64)0);
        ddr_misses_val    = pmm_misses_val    = ((u64)0);
        ddr_misses_tot    = pmm_misses_tot    = ((u64)0);
        ddr_misses_scaled = pmm_misses_scaled = 0.0;

        array_traverse(flat_profile, reg_addr) {
            ddr_misses_val    += reg_addr->region.llc_misses_val[DDR_MISS_EVENT];
            pmm_misses_val    += reg_addr->region.llc_misses_val[PMM_MISS_EVENT];

            ddr_misses_tot    += reg_addr->region.llc_misses_tot[DDR_MISS_EVENT];
            pmm_misses_tot    += reg_addr->region.llc_misses_tot[PMM_MISS_EVENT];

            ddr_misses_scaled += reg_addr->region.llc_misses_scaled[DDR_MISS_EVENT];
            pmm_misses_scaled += reg_addr->region.llc_misses_scaled[PMM_MISS_EVENT];

            val_alloc_ddr     += reg_addr->region.alloc_pages_val[DDR_NODE];
            val_alloc_pmm     += reg_addr->region.alloc_pages_val[PMM_NODE];

            val_free_ddr      += reg_addr->region.free_pages_val[DDR_NODE];
            val_free_pmm      += reg_addr->region.free_pages_val[PMM_NODE];

            total_alloc_ddr   += reg_addr->region.alloc_pages_tot[DDR_NODE];
            total_alloc_pmm   += reg_addr->region.alloc_pages_tot[PMM_NODE];

            total_free_ddr    += reg_addr->region.free_pages_tot[DDR_NODE];
            total_free_pmm    += reg_addr->region.free_pages_tot[PMM_NODE];

            total_regions += 1;
        }
        llc_misses_val    = (ddr_misses_val + pmm_misses_val);
        llc_misses_tot    = (ddr_misses_tot + pmm_misses_tot);
        llc_misses_scaled = (ddr_misses_scaled + pmm_misses_scaled);

        total_rss_ddr     = safe_diff(total_alloc_ddr, total_free_ddr);
        total_rss_pmm     = safe_diff(total_alloc_pmm, total_free_pmm);

        fprintf(profile_file,
                "    regions: %-16lu"
                  "         DDR(MB): %-13.4f"
                  "         PMM(MB): %-13.4f\n",
                total_regions, pages_to_MB(total_rss_ddr), pages_to_MB(total_rss_pmm) );

        fprintf(profile_file,
                "    val_alloc_ddr:   %15lu"
                  "  val_free_ddr:    %15lu\n",
                val_alloc_ddr, val_free_ddr);

        fprintf(profile_file,
                "    val_alloc_pmm:   %15lu"
                  "  val_free_pmm:    %15lu\n",
                val_alloc_pmm, val_free_pmm);

        fprintf(profile_file,
                "    total_alloc_ddr: %15lu"
                  "  total_free_ddr:  %15lu\n",
                total_alloc_ddr, total_free_ddr);

        fprintf(profile_file,
                "    total_alloc_pmm: %15lu"
                  "  total_free_pmm:  %15lu\n",
                total_alloc_pmm, total_free_pmm);

        fprintf(profile_file,
                "    ddr_misses_scaled: %13.4f"
                  "  pmm_misses_scaled: %13.4f"
                  "  llc_misses_scaled: %13.4f\n",
                ddr_misses_scaled, pmm_misses_scaled, llc_misses_scaled);

        fprintf(profile_file,
                "    ddr_misses_val: %16lu"
                  "  pmm_misses_val: %16lu"
                  "  llc_misses_val: %16lu\n",
                ddr_misses_val, pmm_misses_val, llc_misses_val);

        fprintf(profile_file,
                "    ddr_misses_tot: %16lu"
                  "  pmm_misses_tot: %16lu"
                  "  llc_misses_tot: %16lu\n",
                ddr_misses_tot, pmm_misses_tot, llc_misses_tot);

        if ( (total_regions == 0)   ||
             (llc_misses_tot == 0)  || 
             ((env->print_region_limit) < 0.0001) ||
             ((env->print_misses_limit) < 0.0001) ) {
            continue; 
        }

        cur_regions = ((u64)0);
        cur_misses_scaled = 0.0;
        cur_regions_ratio = cur_misses_ratio = 0.0;
        array_traverse(flat_profile, reg_addr) {
            if ( (cur_regions_ratio > (env->print_region_limit)) ||
                 (cur_misses_ratio  > (env->print_misses_limit)) ) {
                break;
            }

            ddr_misses_scaled = reg_addr->region.llc_misses_scaled[DDR_MISS_EVENT];
            pmm_misses_scaled = reg_addr->region.llc_misses_scaled[PMM_MISS_EVENT];
            reg_misses_scaled = (ddr_misses_scaled + pmm_misses_scaled);

            reg_rss_ddr       = safe_diff( reg_addr->region.alloc_pages_tot[DDR_NODE], 
                                           reg_addr->region.free_pages_tot[DDR_NODE] );

            reg_rss_pmm       = safe_diff( reg_addr->region.alloc_pages_tot[PMM_NODE], 
                                           reg_addr->region.free_pages_tot[PMM_NODE] );

            fprintf(profile_file,
                    "      ADDR: %-16lxDDR_ACC: %-12.4fPMM_ACC: %-12.4f"
                          "DDR_RSS: %-12.4fPMM_RSS: %-12.4f\n",
                    (reg_addr->addr << MEMPROF_REGION_SHIFT),
                    ddr_misses_scaled, pmm_misses_scaled,
                    pages_to_MB(reg_rss_ddr), pages_to_MB(reg_rss_pmm));

            cur_regions       += 1;
            cur_misses_scaled += reg_misses_scaled;
            cur_regions_ratio  = ((float)cur_regions / total_regions);
            cur_misses_ratio   = ((float)cur_misses_scaled  / llc_misses_scaled);
        }
        fprintf(profile_file, "\n");
    }
    pthread_mutex_unlock(&mp_profile_lock);
    return;
}

static void add_interval_to_profile(bool reset) {

    mp_profile_map_it it; 

    /* this holds the lock for longer than necessary -- but that's OK because
     * there is not much contention on this lock (right now at least)
     */
    pthread_mutex_lock(&mp_profile_lock);
    tree_traverse(mp_profile, it) {
        add_bpf_region_map_to_profile(tree_it_key(it), reset);
    }
    pthread_mutex_unlock(&mp_profile_lock);
}

static void *mp_profile_entry (void *args) {
    int  profile_val;
    int  profile_print_cnt;
    int  profile_reset_cnt;
    bool reset;
    u64  val_start;
    u64  val_elapsed;
    u64  profile_start;

#if 0
    // for debugging with bpf_printk
    FILE *trace_pipe;
    int trace_fd, flags, err;
    char line[LINE_SIZE];

    trace_pipe = fopen("/sys/kernel/debug/tracing/trace_pipe", "r");
    trace_fd = fileno(trace_pipe);
    flags = fcntl(trace_fd, F_GETFL, 0); 
    flags |= O_NONBLOCK; 
    fcntl(trace_fd, F_SETFL, flags); 
#endif
 
    env = (struct mp_env*) args;
    profile_file = fopen(env->profile_file_string, "w");
    if (!profile_file) {
        fprintf(stderr, "error opening profile file: %s\n%s\n",
                env->profile_file_string, strerror(errno));
        return NULL;
    }

    profile_val = profile_print_cnt = profile_reset_cnt = 0;
    profile_start = val_start = measure_time_now_ms();
    for (;;) {
#if 0
        err = reset_debug_control(10);
        if (err) {
            exit(err);
        }
#endif
        val_elapsed = measure_time_now_ms() - val_start;
        if (val_elapsed < env->profile_period_ms) {
            usleep(1000 * (env->profile_period_ms - val_elapsed));
        } else {
            fprintf(stderr, "[!] profiling took longer than profile-period-ms\n");
        }
        val_start = measure_time_now_ms();

#if 0
        // for debugging with bpf_printk
        while (fgets(line, LINE_SIZE, trace_pipe)) {
            printf("%s", line);
        }
#endif

        reset = (profile_reset_cnt == env->reset_profile_val);
        add_interval_to_profile(reset);

        if (reset) {
            profile_reset_cnt = 0;
        }

        if (profile_print_cnt == env->print_profile_val) {
            print_profile(profile_val, (val_start-profile_start));
            profile_print_cnt = 0;
        }

        profile_reset_cnt += 1;
        profile_print_cnt += 1;
        profile_val += 1;
    }

    return NULL;
}

int start_mp_profile_thread(pthread_t *thread, struct mp_env *env_ptr) {
    int status;
    status = pthread_create(thread, NULL, mp_profile_entry, env_ptr);
    if (status != 0) {
        fprintf(stderr, "failed to create profiling thread\n");
    }
    return status;
}
