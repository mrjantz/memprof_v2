#include <argp.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <linux/perf_event.h>
#include <perfmon/pfmlib_perf_event.h>
#include <asm/unistd.h>
#include <bpf/libbpf.h>
#include <bpf/bpf.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include "memprof.h"
#include "memprof.skel.h"
#include "trace_helpers.h"
#include "map_helpers.h"
#include "mp_profile.h"

#define LINE_SIZE 512
#define PROFILE_FILE "./profile.txt"
#define DDR_MISS_EVENT_STR "MEM_LOAD_UOPS_LLC_MISS_RETIRED.LOCAL_DRAM"
#define PMM_MISS_EVENT_STR "MEM_LOAD_UOPS_RETIRED.LOCAL_PMM"

struct mp_env env = {
	.profile_period_ms = 200,
	.profile_overflow_thresh = 1024,
  .print_profile_val = 50,
  .reset_profile_val = 1000,
  .nr_cpus = 0,
  .alpha = 0.1,
  .print_region_limit = 0.2,
  .print_misses_limit = 1.0,
  .profile_file_string = PROFILE_FILE
};

const char *argp_program_version = "memprof 0.1";
const char argp_program_doc[] =
"Collect profiles of memory usage for virtual address regions.\n"
"\n"
"OPTIONS:";

static const struct argp_option opts[] = {
	{ "profile_period_ms", 'c', "PROFILE_PERIOD_MS", 0,
    "Collect profile every this many ms" },
	{ "profile_overflow_thresh", 's', "PROFILE_OVERFLOW_THRESH", 0,
    "Sampling period for perf" },
	{ "print_profile_val", 'p', "PRINT_PROFILE_VAL", 0,
    "Print profile every this many profile periods" },
	{ "reset_profile_val", 'r', "RESET_PROFILE_VAL", 0,
    "Reset profile every this many profile periods" },
	{ "alpha", 'a', "ALPHA", 0,
    "How much to weight previous profiles when estimating behavior of next profile period" },
	{ "print_region_limit", 'l', "PRINT_REGION_LIMIT", 0,
    "Max number of regions to print at each interval" },
	{ "print_misses_limit", 'm', "PRINT_MISSES_LIMIT", 0,
    "Max number of misses to print at each interval" },
	{ "profile_file_string", 'o', "PROFILE_FILE_STRING", 0,
    "output file for collected profile" },
	{},
};

static error_t parse_arg(int key, char *arg, struct argp_state *state)
{
	switch (key) {
	case 'c':
		errno = 0;
		env.profile_period_ms = strtol(arg, NULL, 10);
		if (errno) {
			fprintf(stderr, "invalid profile period\n");
			argp_usage(state);
		}
		break;
	case 's':
		errno = 0;
		env.profile_overflow_thresh = strtol(arg, NULL, 10);
		if (errno) {
			fprintf(stderr, "invalid profile overflow threshold\n");
			argp_usage(state);
		}
		break;
	case 'p':
		errno = 0;
		env.print_profile_val = strtol(arg, NULL, 10);
		if (errno) {
			fprintf(stderr, "invalid print profile val\n");
			argp_usage(state);
		}
		break;
	case 'r':
		errno = 0;
		env.reset_profile_val = strtol(arg, NULL, 10);
		if (errno) {
			fprintf(stderr, "invalid reset profile val\n");
			argp_usage(state);
		}
		break;
	case 'a':
		errno = 0;
		env.alpha = strtof(arg, NULL);
		if (errno) {
			fprintf(stderr, "invalid alpha\n");
			argp_usage(state);
		}
		break;
	case 'l':
		errno = 0;
		env.print_region_limit = strtof(arg, NULL);
		if (errno) {
			fprintf(stderr, "invalid print region limit\n");
			argp_usage(state);
		}
		break;
	case 'm':
		errno = 0;
		env.print_misses_limit = strtof(arg, NULL);
		if (errno) {
			fprintf(stderr, "invalid print misses limit\n");
			argp_usage(state);
		}
		break;
	case 'o':
		env.profile_file_string = arg;
		break;
	default:
		return ARGP_ERR_UNKNOWN;
	}
	return 0;
}

static int open_and_attach_perf_event(struct perf_event_attr *attr,
				struct bpf_program *prog,
				struct bpf_link *links[])
{
	int i, fd;

	for (i = 0; i < env.nr_cpus; i++) {
		fd = syscall(__NR_perf_event_open, attr, -1, i, -1, 0);
		if (fd < 0) {
			fprintf(stderr, "failed to init perf sampling: %s\n",
				strerror(errno));
			return -1;
		}
		links[i] = bpf_program__attach_perf_event(prog, fd);
		if (libbpf_get_error(links[i])) {
			fprintf(stderr, "failed to attach perf event on cpu: "
				"%d\n", i);
			links[i] = NULL;
			close(fd);
			return -1;
		}
	}
	return 0;
}

static int get_perf_event_attr(const char *event_str, 
    struct perf_event_attr *pe) {
    int err;
    pfm_perf_encode_arg_t pfm;

    memset(pe, 0, sizeof(struct perf_event_attr));
    memset(&pfm, 0, sizeof(pfm_perf_encode_arg_t));

    /* perf_event_open */
    pe->size = sizeof(struct perf_event_attr);
    pe->exclude_kernel = 1;
    pe->exclude_hv     = 1;
    pe->precise_ip     = 2;
    pe->task           = 1;
    pe->use_clockid    = 1;
    pe->clockid        = CLOCK_MONOTONIC_RAW;
    pe->sample_period  = env.profile_overflow_thresh;
    pe->disabled       = 1;

    pe->sample_type  = PERF_SAMPLE_TID;
    pe->sample_type |= PERF_SAMPLE_TIME;
    pe->sample_type |= PERF_SAMPLE_ADDR;
    pe->sample_type |= PERF_SAMPLE_PHYS_ADDR;

    pfm.size = sizeof(pfm_perf_encode_arg_t);
    pfm.attr = pe;

    err = pfm_get_os_event_encoding(event_str, PFM_PLM2 | PFM_PLM3,
          PFM_OS_PERF_EVENT, &pfm);

    if (err != PFM_SUCCESS) {
        fprintf(stderr, "Couldn't find an appropriate event to use."
                "(error %d) Aborting.\n", err);
        return -EINVAL;
    }

    return 0;
}

static int open_and_load_memprof(struct memprof_bpf **obj) {
    struct bpf_map *bpf_procs;
    int err, region_map_fd;

    printf("opening memprof ... ");
    (*obj) = memprof_bpf__open();
    if (!(*obj)) {
        fprintf(stderr, "failed to open BPF object\n");
        return 1;
    }
    printf("done.\n");

    printf("setting bpf_procs ... ");
    bpf_procs = bpf_object__find_map_by_name((*obj)->obj, "bpf_procs");
    if (bpf_procs == NULL) {
        fprintf(stderr, "Failed to find bpf_procs map!\n");
        err = -1;
        goto cleanup;
    }

    region_map_fd = bpf_create_map( BPF_MAP_TYPE_PERCPU_HASH,
        sizeof(__u64), sizeof(struct bpf_region_record),
        MAX_REGIONS, 0 );

    if (bpf_map__set_inner_map_fd(bpf_procs, region_map_fd) != 0) {
        close(region_map_fd);
        fprintf(stderr, "Failed to set inner map for bpf_procs!\n");
        err = -1;
        goto cleanup;
    }
    printf("done.\n");

    printf("loading memprof ... ");
    err = memprof_bpf__load((*obj));
    if (err) {
        fprintf(stderr, "failed to load BPF object: %d\n", err);
        goto cleanup;
    }
    printf("done.\n");

    printf("attaching memprof kprobes ... ");
    err = memprof_bpf__attach((*obj));
    if (err) {
        fprintf(stderr, "failed to attach BPF object: %d\n", err);
        goto cleanup;
    }
    printf("done.\n");
    close(region_map_fd);
    return 0;

cleanup:
    memprof_bpf__destroy((*obj));
    (*obj) = NULL;
    return err != 0;
}

static int init_profiling(struct memprof_bpf **obj) {
    int i, err;
    struct perf_event_attr  pe;
    struct bpf_link **ddr_links, **pmm_links;

    err = bump_memlock_rlimit();
    if (err) {
        fprintf(stderr, "failed to increase rlimit: %d\n", err);
        return err;
    }

    err = open_and_load_memprof(obj);
    if (err) {
        fprintf(stderr, "failed to open and load memprof\n");
        return err;
    }

    env.nr_cpus = libbpf_num_possible_cpus();
    ddr_links = calloc(env.nr_cpus, sizeof(*ddr_links));
    pmm_links = calloc(env.nr_cpus, sizeof(*pmm_links));
    if (!ddr_links || !pmm_links) {
        fprintf(stderr, "failed to alloc ddr_links or pmm_links\n");
        goto cleanup;
    }

    printf("attaching ddr event ... ");
    pfm_initialize();
    if (get_perf_event_attr(DDR_MISS_EVENT_STR, &pe)) {
        fprintf(stderr, "failed to get attr for event: %s\n", DDR_MISS_EVENT_STR);
        goto cleanup;
    }
    printf("done.\n");

    err = open_and_attach_perf_event(&pe, (*obj)->progs.on_ddr_miss, ddr_links);
    if (err) {
        fprintf(stderr, "failed to attach perf event: %s\n", DDR_MISS_EVENT_STR);
        goto cleanup;
    }

    printf("attaching pmm event ... ");
    if (get_perf_event_attr(PMM_MISS_EVENT_STR, &pe)) {
        fprintf(stderr, "failed to get attr for event: %s\n", PMM_MISS_EVENT_STR);
        goto cleanup;
    }

    err = open_and_attach_perf_event(&pe, (*obj)->progs.on_pmm_miss, pmm_links);
    if (err) {
        fprintf(stderr, "failed to attach perf event: %s\n", PMM_MISS_EVENT_STR);
        goto cleanup;
    }
    printf("done.\n");
    return 0;

cleanup:
    for (i = 0; i < env.nr_cpus; i++) {
        bpf_link__destroy(ddr_links[i]);
        bpf_link__destroy(pmm_links[i]);
    }
    free(ddr_links);
    free(pmm_links);
    memprof_bpf__destroy((*obj));
    (*obj) = NULL;
    return err != 0;
}

int close_and_free_region_map_fd( pid_t pid ) {

    fd_and_info         *fdi;
    mp_region_fd_map_it  it;

    it = tree_lookup(region_map_fds, pid);
    if (!(tree_it_good(it))) {
        return -EINVAL;
    }

    fdi = &(tree_it_val(it));
    close(fdi->fd);

    tree_delete(region_map_fds, pid);
    return 0;
}

int create_bpf_region_map( pid_t pid ) {
    char buf[MAP_NAME_SIZE];
    int region_fd;

    if (bpf_procs_fd < 0) {
        fprintf(stderr, "bad bpf_procs!\n");
        return -ENOENT;
    }

    get_region_map_name(pid, &(buf[0]));
    region_fd = bpf_create_map_name( BPF_MAP_TYPE_PERCPU_HASH,
        buf, sizeof(u64), sizeof(struct bpf_region_record),
        MAX_REGIONS, 0 );

    if (region_fd < 0) {
        fprintf(stderr, "Failed to create region map!\n");
        return -ENOMEM;
    }

    if (bpf_map_update_elem(bpf_procs_fd, &pid, &region_fd, 0)) {
        fprintf(stderr, "Failed to insert region map!\n");
        return -EINVAL;
    }

    return region_fd;
}

int add_proc_to_memprof( pid_t pid ) {

    int                  region_fd, err;
    u32                  len;
    fd_and_info          fdi;
    mp_region_map        new_region_map;
    mp_profile_map_it    pit;
    mp_region_fd_map_it  rit;

    err = 0;

    pthread_mutex_lock(&mp_profile_lock);
    rit = tree_lookup(region_map_fds, pid);
    if (tree_it_good(rit)) {
        err = -EEXIST;
        goto out;
    }

    pit = tree_lookup(mp_profile, pid);
    if (tree_it_good(pit)) {
        err = -EEXIST;
        goto out;
    }
    
    region_fd = create_bpf_region_map(pid);
    if (region_fd < 0) {
        err = errno;
        goto out;
    }

    memset(&fdi, 0, sizeof(fdi));
    fdi.fd = region_fd;
    len = sizeof(fdi.info);

    if (bpf_obj_get_info_by_fd(fdi.fd, &(fdi.info), &len) != 0) {
        fprintf(stderr, "can't get map info: %s", strerror(errno));
        err = errno;
        goto out;
    }
    tree_insert(region_map_fds, pid, fdi);

    memset(&(new_region_map), 0, sizeof(mp_region_map));
    new_region_map = tree_make(u64, mp_region);
    tree_insert(mp_profile, pid, new_region_map);

    printf("New region map: "MP_REGION_MAP_STR"\n", pid);
out:
    pthread_mutex_unlock(&mp_profile_lock);
    return err;
}

int remove_bpf_region_map (pid_t pid) {
    if (bpf_procs_fd < 0) {
        return -ENOENT;
    }

    if (bpf_map_delete_elem(bpf_procs_fd, &pid)) {
        return -EEXIST;
    }

    return 0;
}

int remove_proc_from_memprof (pid_t pid) {
    int                err;
    mp_profile_map_it  pit;

    err = 0;

    pthread_mutex_lock(&mp_profile_lock);
    pit = tree_lookup(mp_profile, pid);
    if (!(tree_it_good(pit))) {
        fprintf(stderr, "bad tree lookup\n");
        err = -EEXIST;
        goto out;
    }
    tree_free(tree_it_val(pit));
    tree_delete(mp_profile, tree_it_key(pit));

    err = close_and_free_region_map_fd(pid);
    if (err) {
        fprintf(stderr, "Failed to close region map: "
                MP_REGION_MAP_STR"\n", pid);
        goto out;
    };

    err = remove_bpf_region_map(pid);
    if (err) {
        fprintf(stderr, "Failed to remove region map "
                MP_REGION_MAP_STR"\n", pid);
        goto out;
    };
    printf("Removed region map: "MP_REGION_MAP_STR"\n", pid);

out:
    pthread_mutex_unlock(&mp_profile_lock);
    return err;
}

int send_ack(int err, char *fifo_out) {
    char ack[CMD_SIZE];
    int outfd;

    outfd = open(fifo_out, O_WRONLY);
    if (outfd < 0) {
        fprintf(stderr, "could not open fifo: %s\n", fifo_out);
        return errno;
    }

    sprintf(ack, "%d", err);
    if (write(outfd, ack, (strlen(ack)+1)) < 0) {
        fprintf(stderr, "could not write fifo: %s\n", fifo_out);
        return errno;
    }

    close(outfd);
    return 0;
}

int process_mp_commands() {
    char buf[CMD_SIZE], str[CMD_SIZE];
    int mpfifo_fd;
    pid_t pid;
    int err, bad_cmd, matched;
    enum memprof_cmd cmd;

    err = bad_cmd = 0;
    mpfifo_fd = -1;
    while (true) {
        /* open blocks on named pipes if there is not a process that will
         * write into the pipe
         */
        mpfifo_fd = open(MP_FIFO, O_RDONLY);
        if (mpfifo_fd < 0) {
            printf("could not open %s\n", MP_FIFO);
            return -ENOENT;
        }

        while (read(mpfifo_fd, buf, CMD_SIZE) > 0) {

            matched = sscanf(buf, "%u %d %s", &cmd, &pid, &(str[0]));
            if (matched < 1) { bad_cmd = 1; goto out; }

            switch (cmd) {
            case MP_CMD_CREATE_PROCESS_SYNC:
            case MP_CMD_CREATE_PROCESS_ASYNC:
                if (matched < 2) { bad_cmd = 1; goto out; }
                err = add_proc_to_memprof(pid);
                break;

            case MP_CMD_REMOVE_PROCESS_SYNC:
            case MP_CMD_REMOVE_PROCESS_ASYNC:
                if (matched < 2) { bad_cmd = 1; goto out; }
                err = remove_proc_from_memprof(pid);
                break;

            default:
                bad_cmd = 1;
                goto out;
            }

            switch (cmd) {
            case MP_CMD_CREATE_PROCESS_SYNC:
            case MP_CMD_REMOVE_PROCESS_SYNC:
                if (matched != 3) { bad_cmd = 1; goto out; }
                err |= send_ack(err, str);
                break;
            default:
                break;
            }

            if (err) {
                goto out;
            }
        }

        /* if read returns EOF, all processes connected to the MP_FIFO have
         * exited. For now, we'll close and try to reopen connection with a
         * new process
         */
        close(mpfifo_fd);
        mpfifo_fd = -1;
    }

out:
    if (bad_cmd) {
        fprintf (stderr, "invalid command: %s\n", buf);
        err = -EINVAL;
    }

    if (mpfifo_fd > 0) {
        close(mpfifo_fd);
    }

    return err;
}

int main(int argc, char **argv)
{
    struct memprof_bpf *obj = NULL;
    static const struct argp argp = {
      .options = opts,
      .parser = parse_arg,
      .doc = argp_program_doc,
    };
    int err;
    pthread_t threads[1];

    err = argp_parse(&argp, argc, argv, 0, NULL, NULL);
    if (err) {
        goto out;
    }

    err = init_profiling(&obj);
    if (err) {
        goto out;
    }

    bpf_procs_fd = map_fd_by_name("bpf_procs");
    mp_profile = tree_make(pid_t, mp_region_map);
    region_map_fds = tree_make(pid_t, fd_and_info);
    pthread_mutex_init(&mp_profile_lock, NULL);

    err = start_mp_profile_thread(&(threads[0]), &env); 
    if (err) {
        goto out;
    }

    err = create_fifo(MP_FIFO);
    if (err) {
        goto out;
    }

    printf(":: BPF initialized.\n\n");

    err = process_mp_commands();

    if (unlink(MP_FIFO) < 0) {
        fprintf(stderr, "could not remove fifo: %s\n",
                strerror(errno));
        if (!err) {
            err = errno;
        }
    };

out:
    return err;
}
