#include <argp.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <linux/perf_event.h>
#include <perfmon/pfmlib_perf_event.h>
#include <asm/unistd.h>
#include <bpf/libbpf.h>
#include <bpf/bpf.h>
#include <fcntl.h>
#include <sys/wait.h>

#include "memprof.h"
#include "common.h"

#define OUTPUT_FILE "./out.txt"

struct mp_run_env {
    char *output_file;
};

struct mp_run_env env = {
  .output_file = OUTPUT_FILE
};

const char *argp_program_version = "mp_run 0.1";
const char argp_program_doc[] =
"run application under memprof.\n"
"\n"
"OPTIONS:";

static const struct argp_option opts[] = {
	{ "output_file", 'o', "OUTPUT_FILE", 0,
    "output file for child" },
	{},
};

static error_t parse_arg(int key, char *arg, struct argp_state *state)
{
    switch (key) {
    case 'o':
        env.output_file = arg;
        break;
    default:
        return ARGP_ERR_UNKNOWN;
    }
    return 0;
}

int mp_cmd_sync(int me2mp_fd, char *cmd) {
    int err;
    char __cmd[(CMD_SIZE<<1)], mp2me_str[CMD_SIZE];
    int mp2me_fd;

    // __cmd is double the allowed size to suppress a compiler warning
    sprintf(mp2me_str, MP2ME_FIFO, getpid());
    sprintf(&(__cmd[0]), "%s %s\n", cmd, mp2me_str);
    if ( strlen(__cmd) > CMD_SIZE ) {
        fprintf(stderr, "command is too long: %s", __cmd);
        err = -EINVAL;
        goto out;
    }

    if (write(me2mp_fd, &(__cmd[0]), strlen(&(__cmd[0])) + 1) < 0) {
        fprintf(stderr, "could not write create command\n");
        err = errno;
        goto out;
    }

    mp2me_fd = open(mp2me_str, O_RDONLY);
    if (mp2me_fd < 0) {
        fprintf(stderr, "could not open named pipe: %s\n", mp2me_str);
        err = errno;
        goto out;
    }
 
    if ( (err = read(mp2me_fd, &(__cmd[0]), CMD_SIZE)) <= 0) {
        fprintf(stderr, "could not read ack from memprof\n");
        err = err ? errno : -ENOENT;
        goto close_out;
    }

    err = (atoi(&(__cmd[0])));
close_out:
    close(mp2me_fd);
out:
    return err;
}

int mp_cmd_async(int me2mp_fd, char *cmd) {
    char __cmd[CMD_SIZE];

    sprintf(&(__cmd[0]), "%s\n", cmd);
    if (write(me2mp_fd, &(__cmd[0]), (strlen(cmd)+1)) < 0) {
        fprintf(stderr, "could not write create command: %s\n", __cmd);
        return errno;
    }

    return 0;
}

int me2mp_create_process(int me2mp_fd, pid_t pid, bool sync) {
    int err;
    char cmd[CMD_SIZE];

    if (sync) {
        sprintf(cmd, "%d %d", MP_CMD_CREATE_PROCESS_SYNC, pid);
        err = mp_cmd_sync(me2mp_fd, cmd);
    } else {
        sprintf(cmd, "%d %d", MP_CMD_CREATE_PROCESS_ASYNC, pid);
        err = mp_cmd_async(me2mp_fd, cmd);
    }
    return err;
}

int me2mp_delete_process(int me2mp_fd, pid_t pid, bool sync) {
    int err;
    char cmd[CMD_SIZE];

    if (sync) {
        sprintf(cmd, "%d %d", MP_CMD_REMOVE_PROCESS_SYNC, pid);
        err = mp_cmd_sync(me2mp_fd, cmd);
    } else {
        sprintf(cmd, "%d %d", MP_CMD_REMOVE_PROCESS_ASYNC, pid);
        err = mp_cmd_async(me2mp_fd, cmd);
    }
    return err;
}

int main(int argc, char **argv)
{
    static const struct argp argp = {
      .options = opts,
      .parser = parse_arg,
      .doc = argp_program_doc,
    };
    int err, idx, pid, wstatus;
    char mp2c_fifo[CMD_SIZE];
    int me2mp_fd;

    err = argp_parse(&argp, argc, argv, 0, &idx, NULL);
    if (err) {
        return err;
    }

    me2mp_fd = open(MP_FIFO, O_WRONLY);
    if (me2mp_fd < 0) {
        fprintf(stderr, "could not open named pipe: %s\n", MP_FIFO);
        return errno;
    }

    pid = fork();
    if (pid == 0) {
        int out_fd;
        char mp2me_str[CMD_SIZE];

        sprintf(mp2me_str, MP2ME_FIFO, getpid());
        err = create_fifo(mp2me_str);
        if (err) {
            exit(errno);
        }
 
        err = me2mp_create_process(me2mp_fd, getpid(), true);
        if (err) {
            fprintf(stderr, "could not create process region map\n");
            exit(err);
        }

        out_fd = open(env.output_file, (O_RDWR | O_CREAT | O_TRUNC),
                     S_IRUSR | S_IWUSR);
        if (out_fd < 0) {
            fprintf(stderr, strerror(errno));
            exit(errno);
        }

        dup2(out_fd, 1);
        dup2(out_fd, 2);
        close(out_fd);
        close(me2mp_fd);

        err = execvp(argv[idx], &(argv[idx]));
        if (err) {
            fprintf (stderr, "could not exec program: %s %s",
                     argv[idx], strerror(errno));
            exit(errno);
        }
    }


	  do {
		    err = waitpid(pid, &wstatus, WUNTRACED | WCONTINUED);
        if (err == -1) {
            perror("waitpid");
            exit(EXIT_FAILURE);
        }

        if (WIFEXITED(wstatus)) {
            printf("exited, status=%d\n", WEXITSTATUS(wstatus));
        } else if (WIFSIGNALED(wstatus)) {
            printf("killed by signal %d\n", WTERMSIG(wstatus));
        }
    } while (!WIFEXITED(wstatus) && !WIFSIGNALED(wstatus));

    err = me2mp_delete_process(me2mp_fd, pid, false);
    if (err) {
        fprintf(stderr, "error deleting process %d from memprof: %d\n", pid, err);
        exit(err);
    }

    sprintf(mp2c_fifo, MP2ME_FIFO, pid);
    err = unlink(mp2c_fifo);
    if (err) {
        fprintf(stderr, "could not unlink child fifo: %s\n", mp2c_fifo);
        exit(errno);
    }
    close(me2mp_fd);

    return 0;
}

