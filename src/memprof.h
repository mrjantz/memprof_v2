#ifndef __MEMPROF_H
#define __MEMPROF_H

#define MAX_PROCS             4
#define MAX_REGIONS           20480
#define MEMPROF_REGION_SHIFT  21
#define MP_FIFO               "/tmp/mpfifo"
#define MP2ME_FIFO            "/tmp/mpfifo_%d"

// per POSIX.1 -- writes to pipes less than 4KB are atomic
// So CMD_SIZE must be less than 4KB for this arch to work
#define CMD_SIZE              64
#define MAP_NAME_SIZE         16
#define MP_REGION_MAP_STR     "mp_reg_%u"

enum llc_miss_event {
    DDR_MISS_EVENT,
    PMM_MISS_EVENT,
    NR_MISS_EVENTS
};

enum numa_node_id {
    DDR_NODE,
    PMM_NODE,
    NR_NUMA_NODES
};

enum memprof_cmd {
    MP_CMD_CREATE_PROCESS_ASYNC,
    MP_CMD_REMOVE_PROCESS_ASYNC,
    MP_CMD_CREATE_PROCESS_SYNC,
    MP_CMD_REMOVE_PROCESS_SYNC
};

struct mp_env {
  int profile_period_ms;
	int profile_overflow_thresh;
  int print_profile_val;
  int reset_profile_val;
  int nr_cpus;
  float alpha;
  float print_region_limit;
  float print_misses_limit;
  char *profile_file_string;
};

struct bpf_region_record {
    __u64 llc_misses[NR_MISS_EVENTS];
    __u64 alloc_pages[NR_NUMA_NODES];
    __u64 free_pages[NR_NUMA_NODES];
};

#endif /* __MEMPROF_H */
