# memprof\_v2: just like memprof -- with a few differences:
#   written entirely in C
#   solves problem of adding bpf maps dynamically
#   since it uses static linking with libbpf -- could theoretically be
#     installed and run on a platform without the Linux, clang, bcc dependencies,
#     but I haven't tested this

